'use strict';

var fbReach = document.querySelector('.facebook-reach');
var twReach = document.querySelector('.twitter-reach');
var inReach = document.querySelector('.instagram-reach');
var ytReach = document.querySelector('.youtube-reach');
var minReach = 0;
var maxReach = 10000;
var fbData, inData, ytData, uominiData, donneData = [];

//Getting social Reaches

$.ajax({
    url: 'http://localhost:3004/reaches',
    data: {
        format: 'json'
    },
    error: function() {
        $('#infoReach').html('<p>An error has occurred</p>');
    },
    dataType: 'jsonp',
    success: function(data) {

        let fbReachVal = data.facebook.points;
        let twReachVal = data.twitter.points;
        let inReachVal = data.instagram.points;
        let ytReachVal = data.youtube.points;

        fbReach.innerText = fbReachVal;
        twReach.innerText = twReachVal;
        inReach.innerText = inReachVal;
        ytReach.innerText = ytReachVal;


        //Facebook
        let fbPercentage = ((fbReachVal - minReach) * 100) / (maxReach - minReach);
        fbPercentage = fbPercentage.toFixed(2);
        let fbElement = document.querySelector('.fb-percentage');
        let fbTotal = document.querySelector('.fb-total');
        //Twitter
        let twPercentage = ((twReachVal - minReach) * 100) / (maxReach - minReach);
        twPercentage = twPercentage.toFixed(2);
        let twElement = document.querySelector('.tw-percentage');
        let twTotal = document.querySelector('.tw-total');

        //Instagram
        let inPercentage = ((inReachVal - minReach) * 100) / (maxReach - minReach);
        inPercentage = inPercentage.toFixed(2);
        let inElement = document.querySelector('.in-percentage');
        let inTotal = document.querySelector('.in-total');
        //Youtube
        let ytPercentage = ((ytReachVal - minReach) * 100) / (maxReach - minReach);
        ytPercentage = ytPercentage.toFixed(2);
        let ytElement = document.querySelector('.yt-percentage');
        let ytTotal = document.querySelector('.yt-total');

        fbTotal.innerText = fbPercentage;
        fbElement.style.width = fbPercentage + '%';

        twTotal.innerText = twPercentage;
        twElement.style.width = twPercentage + '%';

        inTotal.innerText = inPercentage;
        inElement.style.width = inPercentage + '%';

        ytTotal.innerText = ytPercentage;
        ytElement.style.width = ytPercentage + '%';


    },
    type: 'GET'
});

//Getting Engagement

$( "#engTab" ).click(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost:3004/socials',
        data: {
            format: 'json'
        },
        error: function() {
            $('#infoEngage').html('<p>An error has occurred</p>');
        },
        dataType: 'jsonp',
        success: function(data) {
            fbData = data.facebook.points;
            inData = data.instagram.points;
            ytData = data.youtube.points;
            loadChart(fbData, inData, ytData);
        },
        type: 'GET'
    });
});

$( "#tarTab" ).click(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'http://localhost:3004/targets',
        data: {
            format: 'json'
        },
        error: function() {
            $('#infoTarget').html('<p>An error has occurred</p>');
        },
        dataType: 'jsonp',
        success: function(data) {
            uominiData = data.uomini.points;
            donneData = data.donne.points;
            loadChartTarget(uominiData, donneData);
        },
        type: 'GET'
    });
});

var loadChart = function (fbData, inData, ytData) {
    var ctx = document.getElementById("engagementChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["1 Giugno", "2 Giugno", "3 Giugno", "4 Giugno", "5 Giugno", "6 Giugno", "7 Giugno"],
            datasets: [{
                label: 'Facebook',
                data: fbData,
                backgroundColor: [
                    'rgba(59, 89, 153, 0)',
                    'rgba(54, 162, 235, 0)',
                    'rgba(255, 206, 86, 0)',
                    'rgba(75, 192, 192, 0)',
                    'rgba(153, 102, 255, 0)',
                    'rgba(255, 159, 64, 0)'
                ],
                borderColor: [
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)'
                ],
                pointBackgroundColor: [
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)',
                    'rgba(59, 89, 153, 1)'
                ],
                borderWidth: 1
            },
                {
                    label: 'Instagram',
                    data: inData,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0)',
                        'rgba(54, 162, 235, 0)',
                        'rgba(255, 206, 86, 0)',
                        'rgba(75, 192, 192, 0)',
                        'rgba(153, 102, 255, 0)',
                        'rgba(255, 159, 64, 0)'
                    ],
                    borderColor: [
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)'
                    ],
                    pointBackgroundColor: [
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)',
                        'rgba(252,175,69,1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: 'Youtube',
                    data: ytData,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0)',
                        'rgba(54, 162, 235, 0)',
                        'rgba(255, 206, 86, 0)',
                        'rgba(75, 192, 192, 0)',
                        'rgba(153, 102, 255, 0)',
                        'rgba(255, 159, 64, 0)'
                    ],
                    borderColor: [
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)'
                    ],
                    pointBackgroundColor: [
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)',
                        'rgba(207,32,29,1)'
                    ],
                    borderWidth: 1
                }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    gridLines: {
                        display: true
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }]
            },
            legend: {
                display: false
            },
            animation: {
                easing: 'linear'
            },
            tooltips: {
                // Disable the on-canvas tooltip
                enabled: false,

                custom: function(tooltip) {
                    var tooltipEl = $('#chartjs-tooltip');

                    if (!tooltip) {
                        tooltipEl.css({
                            opacity: 0
                        });
                        return;
                    }

                    if(tooltip.body){ // if the cursor hits the point
                       let value = tooltip.body[0].lines; //the data i need to add on my custom tooltip
                        let imgTool = '<img src="/img/eye.svg"/>';
                        tooltipEl.html('<div><p style="margin: 0">Soials: '+value+'</p><div>'+imgTool+'</div></div>'); //icon is the html tag <i class="fa fa-sampleicon"></i>

                        //set the custom div to where the default tooltip is supposed to show.
                        tooltipEl.css({
                            opacity: 1,
                            left: tooltip.x + 'px',
                            top: tooltip.y + 'px',
                            fontFamily: this.fontFamily,
                            fontSize: this.fontSize,
                            fontStyle: this.fontStyle,
                        });
                        $("#engagementChart").css("cursor", "pointer");
                    }
                    else
                    {
                        // if outside of the point, it'll hide the custom div
                        tooltipEl.css({
                            opacity: 0
                        });
                        //change my cursor to default
                        $("#engagementChart").css("cursor", "default");
                    }
                }
            }
        }
    });
};
var loadChartTarget = function (uominiData, donneData) {
    var cty = document.getElementById("targetChart");
    var tarChart = new Chart(cty, {
        type: 'bar',
        data: {
            labels: ["13 - 17 anni", "18 -24 anni", "25 - 34 anni", "35 - 44 anni",  "45 - 54 anni", "55 - 64 anni", "+65 anni"],
            datasets: [
                {
                    label: "Uomini",
                    backgroundColor: ["#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd"],
                    data: uominiData
                },
                {
                    label: "Donne",
                    backgroundColor: ["#ea4c89", "#ea4c89", "#ea4c89", "#ea4c89", "#ea4c89", "#ea4c89", "#ea4c89"],
                    data: donneData
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: false
            },
            maintainAspectRatio: false,
            responsive: true,
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        display: false
                    },
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false
                    },
                    barPercentage: 0.5,
                    categoryPercentage: 0.1,

                }]
            }
        }
    });
};